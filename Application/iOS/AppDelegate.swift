//
//  AppDelegate.swift
//  Monumento
//
//  Created by Aaryan Kothari on 30/06/21.
//

import Foundation
import Firebase
import GoogleSignIn

class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        return true
    }
    
}
