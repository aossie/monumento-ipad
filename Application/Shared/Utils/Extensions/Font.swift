//
//  Font.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 28/06/21.
//

import Foundation
import SwiftUI

extension Font {
    static let header = Font.system(size: 55, weight: .bold, design: .default)
    static let subHeader = Font.system(size: 20, weight: .bold, design: .default)
}
