//
//  Alert.swift
//  Monumento
//
//  Created by Aaryan Kothari on 01/07/21.
//

import SwiftUI

extension Alert {
    static func okayAlert(alert: StringConstants.Alerts) -> Alert {
        return Alert(title: Text(alert.title), message: Text(alert.message), dismissButton: .default(Text("OK")))
    }
}
