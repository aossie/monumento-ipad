//
//  Strings.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 29/06/21.
//

import Foundation

// MARK: - STRING CONSTANTS

struct StringConstants {
    
    enum Placeholders: String {
        case email = "Enter your email"
        case password = "Enter your password"
        case fName = "First Name"
        case lName = "Last Name"
        case userName = "Username"
        case status = "Status"
        case title = "Title"
        case location = "Location"
        case search = "Search People"
    }
    
    enum SystemImages: String {
        case mail = "mail"
        case lock = "lock"
        case person = "person"
        case circlePerson = "person.crop.circle"
        case note = "note.text"
        case mappin = "mappin.circle.fill"
        case pencil = "pencil.circle.fill"
        case map = "map.fill"
        case backArrow = "chevron.left.square.fill"
        case x = "multiply.square.fill"
        case magnifyingGlass = "magnifyingglass.circle"
    }
    
    enum Images: String {
        case apple = "apple"
        case google = "google"
        case comment = "comment"
        case options = "options"
        case save = "save"
        case share = "share"
        case share2 = "share-2"
        case favourites = "favourites"
    }
    
    enum Alerts {
        case loginSuccess
        case error(message:String)
        
        var title: String {
            switch self {
            case .loginSuccess: return "Yay! 😃"
            case .error(let _): return "Uh oh 😕"
            }
        }
        
        var message: String {
            switch self{
            case .loginSuccess: return "Sucessfully logged in"
            case .error(let err): return err
            }
        }
    }
}
