//
//  Enumerations.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 29/06/21.
//

import Foundation
import SwiftUI

enum TextFieldType {
    case email
    case password
    case fName
    case lName
    case username
    case status
    case title
    case location
    case search
    
    var icon: StringConstants.SystemImages {
        switch self {
        case .email: return .mail
        case .password: return .lock
        case .fName: return .person
        case .lName: return .person
        case .username: return .circlePerson
        case .status: return .note
        case .title: return .pencil
        case .location: return .mappin
        case .search: return .magnifyingGlass
        }
    }
    
    var placeHolder: StringConstants.Placeholders {
        switch self {
        case .email: return .email
        case .password: return .password
        case .fName: return .fName
        case .lName: return .lName
        case .username: return .userName
        case .status: return .status
        case .title: return .title
        case .location: return .location
        case .search: return .location
        }
    }
}

enum SocialButtonType {
    case apple
    case google
    
    var title: String {
        switch self {
        case .apple: return "Continue with Apple"
        case .google: return "Continue with Google"
        }
    }
    
    var icon: String {
        switch self {
        case .apple: return StringConstants.Images.apple.rawValue
        case .google: return StringConstants.Images.google.rawValue
        }
    }
}
