//
//  MonumentService.swift
//  Monumento
//
//  Created by Aaryan Kothari on 15/07/21.
//

import Firebase
import Combine

class MonumentService: ObservableObject {
    private let monumentCollection = Firestore.firestore().collection("monuments")
    
    private var cancellables: [AnyCancellable] = []
    
    let onErrorCompletion: ((Subscribers.Completion<Error>) -> Void) = { completion in
            switch completion {
            case .finished: print("🏁 finished")
            case .failure(let error): print("❗️ failure: \(error)")
            }
        }
    
    let onValue = { print("✅ value") }
    
    func fetchMonuments(completion: @escaping ([Monument]) -> ()) {
        monumentCollection
            .getDocuments(as: Monument.self)
            .sink(receiveCompletion: onErrorCompletion, receiveValue: { print($0);completion($0) } )
            .store(in: &cancellables)
    }

}
