//
//  SocialService.swift
//  Monumento
//
//  Created by Aaryan Kothari on 30/06/21.
//


import SwiftUI
import GoogleSignIn
import AuthenticationServices
import Firebase

// MARK: - SIGNIN WITH GOOGLE COORDINATOR

class GoogleCoordinator: NSObject, GIDSignInDelegate {
    
    var session: SessionStore
    
    init(session: SessionStore) {
        self.session = session
        super.init()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
    }
    
    // google sign in flow
    func googleSignin() {
        GIDSignIn.sharedInstance()?.presentingViewController = UIApplication.shared.windows.last?.rootViewController
        GIDSignIn.sharedInstance()?.signIn()
    }
    
}

extension GoogleCoordinator  {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            session.showAlert = true
            session.alert = .error(message: error.localizedDescription)
            return
        }
        
        let credential = GoogleAuthProvider.credential(withIDToken: user.authentication.idToken, accessToken: user.authentication.accessToken)
        session.socialSignin(with: credential)
        
    }
}
