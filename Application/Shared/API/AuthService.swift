//
//  AuthService.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 30/06/21.
//

import Combine
import Firebase

// MARK: - FIREBASE AUTHENTICATION NETWORKING CLASS

final class AuthService: AuthServiceProtocol {

    static let sharedInstance : AuthService = AuthService()
    
    var currentUser: Firebase.User?

    /// LOGIN EXISTISTING USER WITH EMAIL AND PASSWORD
    class func login(email: String, password: String) -> AnyPublisher<String, Error> {
        Future<String, Error> { promise in
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if let error = error {
                    promise(.failure(error))
                } else {
                    promise(.success(result?.user.uid ?? ""))
                }
            }
        }.eraseToAnyPublisher()
    }

    /// CREATE NEW USER WITH EMAIL AND PASSWORD
    class func signup(email: String, password: String) -> AnyPublisher<String, Error> {
        Future<String, Error> { promise in
            Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
                if let error = error {
                    promise(.failure(error))
                } else {
                    promise(.success(result?.user.uid ?? ""))
                }
            }
        }.eraseToAnyPublisher()
    }
    
    /// SOCIAL SIGNIN GOOGLE / APPLE
    class func socialSignin(with credential :AuthCredential) -> AnyPublisher<String, Error> {
        Future<String, Error> { promise in
        Auth.auth().signIn(with: credential) { (result, error) in
            if let error = error {
                promise(.failure(error))
            } else {
                promise(.success(result?.user.uid ?? ""))
            }
        }
        }.eraseToAnyPublisher()
    }
    
    /// LOGOUT
    class func logout() -> AnyPublisher<Void, Error> {
        Future<Void, Error> { promise in
            do {
                try Auth.auth().signOut()
                promise(.success(()))
            } catch (let error) {
                promise(.failure(error))
            }
        }.eraseToAnyPublisher()
    }


}




