//
//  User.swift
//  Monumento
//
//  Created by Aaryan Kothari on 30/06/21.
//

import Foundation

typealias SuccessErrorCallback = (_ success: Bool, _ error: String?) -> Void


class User: Codable,Cachable, Identifiable {
    
    static let cacheFileName = "User.json"
    
    static var cache: User? {
        if let user: User = Cacher(destination: .jsonCache).load(fileName: User.cacheFileName) {
            return user
        }
        return nil
    }
    
    static var current: User {
        get {
            return User.cache ?? User(firstName: "Aaryan", lastName: "Kothari", username: "aaryan.kotharii", status: "YOLO", profilePicture: "https://cdn.mos.cms.futurecdn.net/wtqqnkYDYi2ifsWZVW2MT4-1200-80.jpg")
        }
    }
    
    var fileName: String {
        return User.cacheFileName
    }
    
    var id: String {
        return uid ?? UUID().uuidString
    }
    
    var uid: String?
    var email: String?
    var firstName: String?
    var lastName: String?
    var username: String?
    var status: String?
    var name: String?
    var following: [String]?
    var profilePicture: String?
    
    enum CodingKeys: String, CodingKey {
        case uid
        case email
        case username
        case status
        case name
        case following
        case profilePicture = "profilePictureUrl"
    }
    
    init() { }
    
    init(uid: String? = nil, email: String? = nil, firstName: String, lastName: String, username: String, status: String, profilePicture: String? = nil) {
        self.uid = uid
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.username = username
        self.status = status
        self.profilePicture = profilePicture
    }
    
    var asDictionary: [String:String] {
        var dict: [String:String] = [:]
        if let uid = uid { dict["uid"] = uid }
        if let email = email { dict["email"] = email }
        dict["name"] = (firstName ?? "") + " " + (lastName ?? "")
        dict["username"] = username
        dict["status"] = status
        return dict
    }
    
    var profilePictureUrl: URL? {
        return URL(string: profilePicture ?? "")
    }
    
    var formattedUserName: String {
        return "@ \(username ?? "")"
    }
    
    func persist(callback: @escaping SuccessErrorCallback) {
        Cacher(destination: .jsonCache).persist(item: self) { (url, error) in
            DispatchQueue.main.async {
                if error == nil {
                    callback(true, nil)
                } else {
                    callback(false, error?.localizedDescription)
                }
            }
        }
    }
    
}
