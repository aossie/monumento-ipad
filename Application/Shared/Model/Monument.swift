//
//  Monument.swift
//  Monumento
//
//  Created by Aaryan Kothari on 15/07/21.
//

import Foundation
import UIKit

struct Monument: Codable,Identifiable {
    var id = UUID()
    var name: String?
    var coordinate: [Double]
    var imageUrlString: String?
    var wikiLink: String?
    var isPopular: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case name
        case coordinate
        case imageUrlString = "image"
        case wikiLink = "wikipediaLink"
    }
    
    var image: URL? {
        return URL(string: imageUrlString ?? "")
    }
    
    var wikiUrl: URL? {
        return URL(string: wikiLink ?? "")
    }
    
    var uiImage: UIImage?
    
    static let data = [
        Monument(name: "Colosseum of Rome", coordinate: [27.173891,78.042068], imageUrlString: "cor", isPopular:true),
        Monument(name: "Colosseum of Rome", coordinate: [], imageUrlString: "cor", isPopular:true),
        Monument(name: "Pyramids of Giza", coordinate: [], imageUrlString: "cor", isPopular:true),
    ]
}
