//
//  PrivacyPolicyLabel.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 09/07/21.
//

import SwiftUI

struct PrivacyPolicyLabel: View {
    var body: some View {
        VStack(spacing:1) {
            Text("by  signing  up  you  are  agreeing  to  our")
            HStack(spacing:1) {
                Link("privacy  policy", destination: URL(string: "https://gitlab.com/aossie/monumento-ipad")!)
                    .foregroundColor(.darkBlue)
                Text("  and  ")
                Link("terms  of  service", destination: URL(string: "https://gitlab.com/aossie/monumento-ipad")!)
                    .foregroundColor(.darkBlue)
            }
        }
    }
}

struct PrivacyPolicyLabel_Previews: PreviewProvider {
    static var previews: some View {
        PrivacyPolicyLabel()
    }
}
