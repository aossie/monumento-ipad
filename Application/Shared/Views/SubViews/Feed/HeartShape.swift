//
//  HeartShape.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 21/07/21.
//

import SwiftUI

struct HeartShape: View {
    @State var tapped: Bool = false
    
    let gradient = LinearGradient(gradient: Gradient(colors: [Color.darkPink,Color.lightPink]), startPoint: .top, endPoint: .bottom)
    
    let empty = LinearGradient(gradient: Gradient(colors: []), startPoint: .top, endPoint: .bottom)
    var body: some View {
       Heart()
        .fill(tapped ? gradient : empty)
        .aspectRatio(1, contentMode: .fill)
        .onTapGesture{
            self.tapped.toggle()
        }
        .animation(.easeInOut)
    }
}

struct HeartShape_Previews: PreviewProvider {
    static var previews: some View {
        HeartShape()
            .previewLayout(.sizeThatFits)
    }
}


struct Heart: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        let width = rect.size.width
        let height = rect.size.height
        path.move(to: CGPoint(x: 0.04152*width, y: 0.47141*height))
        path.addLine(to: CGPoint(x: 0.04015*width, y: 0.46693*height))
        path.addCurve(to: CGPoint(x: 0.2253*width, y: 0.04005*height), control1: CGPoint(x: -0.00856*width, y: 0.3003*height), control2: CGPoint(x: 0.05017*width, y: 0.09932*height))
        path.addLine(to: CGPoint(x: 0.22533*width, y: 0.04004*height))
        path.addCurve(to: CGPoint(x: 0.47467*width, y: 0.0731*height), control1: CGPoint(x: 0.30922*width, y: 0.01154*height), control2: CGPoint(x: 0.40069*width, y: 0.02407*height))
        path.addLine(to: CGPoint(x: 0.48739*width, y: 0.08153*height))
        path.addLine(to: CGPoint(x: 0.5001*width, y: 0.07309*height))
        path.addCurve(to: CGPoint(x: 0.74924*width, y: 0.04006*height), control1: CGPoint(x: 0.57158*width, y: 0.02564*height), control2: CGPoint(x: 0.6671*width, y: 0.01243*height))
        path.addCurve(to: CGPoint(x: 0.93346*width, y: 0.47138*height), control1: CGPoint(x: 0.92614*width, y: 0.09998*height), control2: CGPoint(x: 0.98449*width, y: 0.304*height))
        path.addLine(to: CGPoint(x: 0.93343*width, y: 0.47151*height))
        path.addCurve(to: CGPoint(x: 0.68982*width, y: 0.80587*height), control1: CGPoint(x: 0.89415*width, y: 0.60266*height), control2: CGPoint(x: 0.7895*width, y: 0.7195*height))
        path.addCurve(to: CGPoint(x: 0.4932*width, y: 0.94849*height), control1: CGPoint(x: 0.59079*width, y: 0.89167*height), control2: CGPoint(x: 0.50005*width, y: 0.94454*height))
        path.addLine(to: CGPoint(x: 0.49313*width, y: 0.94854*height))
        path.addCurve(to: CGPoint(x: 0.48752*width, y: 0.95006*height), control1: CGPoint(x: 0.49139*width, y: 0.94955*height), control2: CGPoint(x: 0.48946*width, y: 0.95006*height))
        path.addCurve(to: CGPoint(x: 0.48181*width, y: 0.94855*height), control1: CGPoint(x: 0.48549*width, y: 0.95006*height), control2: CGPoint(x: 0.48356*width, y: 0.94954*height))
        path.addLine(to: CGPoint(x: 0.47043*width, y: 0.9705*height))
        path.addLine(to: CGPoint(x: 0.48184*width, y: 0.94856*height))
        path.addCurve(to: CGPoint(x: 0.28628*width, y: 0.80717*height), control1: CGPoint(x: 0.47503*width, y: 0.94466*height), control2: CGPoint(x: 0.38498*width, y: 0.89259*height))
        path.addCurve(to: CGPoint(x: 0.04152*width, y: 0.47141*height), control1: CGPoint(x: 0.18698*width, y: 0.72123*height), control2: CGPoint(x: 0.08218*width, y: 0.6043*height))
        path.closeSubpath()
        return path
    }
}
