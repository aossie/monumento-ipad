//
//  FeedRow.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 21/07/21.
//

import SwiftUI
import Kingfisher

struct FeedRow: View {
    var post: Post
    @StateObject var postVM: PostViewModel
    @State var present = false
    var body: some View {
        VStack {
            HStack {
                NavigationLink(
                    destination: ProfileView(profileVM: ProfileViewModel(uid: post.author?.uid ?? ""))) {
                    KFImage(post.author?.profilePictureUrl)
                    .resizable()
                    .aspectRatio(1, contentMode: .fit)
                    .clipShape(Circle())
                }
                VStack(alignment:.leading) {
                    Text(post.author?.name ?? "name")
                        .bold()
                    Button(action: {}) {
                        Label(post.location ?? "", systemImage: StringConstants.SystemImages.mappin.rawValue)
                            .frame(maxHeight:40)
                        
                    }
                    .buttonStyle(PlainButtonStyle())
                }
                Spacer()
                Button(action: {}) {
                    Image(StringConstants.Images.options.rawValue)
                        .resizable()
                        .aspectRatio(1, contentMode: .fit)
                }
                .padding(20)
            }
            .frame(maxHeight:75)
            KFImage(post.imageUrl)
                .resizable()
                .cornerRadius(18)
            
            HStack {
                Spacer()
                
                Heart()
                    .stroke(Color.black,lineWidth: 2)
                    .foregroundColor(.clear)
                    
                    .aspectRatio(1, contentMode: .fit)

                
                Spacer()
                
                Image(StringConstants.Images.comment.rawValue)
                    .resizable()
                    .aspectRatio(1, contentMode: .fit)
                    .button {
                        self.present = true
                    }
                
                Spacer()
                
                Image(StringConstants.Images.share.rawValue)
                    .resizable()
                    .aspectRatio(1, contentMode: .fit)
                
               Spacer()
                
                Image(StringConstants.Images.save.rawValue)
                    .resizable()
                    .aspectRatio(0.73, contentMode: .fit)
                
                Spacer()
                
            }
            .padding(5)
            .frame(maxHeight:40)
        }
        .aspectRatio(1.11, contentMode: .fit)
        .frame(maxWidth:800, maxHeight:650)
        .padding(15)
        .background(Color.white.shadow(color: .black.opacity(0.25), radius: 5, x: 0, y: 0))
        .padding(.horizontal,30)
        .sheet(isPresented: $present) {
           CommentsView(postVM: postVM)
        }
    }
}

struct FeedRow_Previews: PreviewProvider {
    static var previews: some View {
        FeedRow(post: Post.data, postVM: PostViewModel(with: Post.data.id))
    }
}
