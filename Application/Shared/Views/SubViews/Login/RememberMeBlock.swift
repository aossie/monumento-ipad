//
//  RememberMeBlock.swift
//  Monumento
//
//  Created by Aaryan Kothari on 29/06/21.
//

import Foundation
import SwiftUI

struct RememberMeBlock: View {
    @State var isSelected: Bool
    @State var color: Color = .gray
    var body: some View {
        HStack {
            Button(action: tapped) {
                ZStack {
                    Text(isSelected ? "✅" : "")
                        .padding(2)
                        .frame(width: 27, height: 27)
                        .roundedBorder(radius: 4, color: color)
                }
            }
        Text("Remember me")
                .font(.body)
                .foregroundColor(color)
                .lineLimit(1)
                .minimumScaleFactor(0.4)
        }
    }
    
    func tapped() {
        self.isSelected.toggle()
        self.color = isSelected ? .green : .gray
    }
}

struct RememberMeBlock_Previews: PreviewProvider {
    static var previews: some View {
        RememberMeBlock(isSelected: false)
            .previewLayout(.sizeThatFits)
    }
}

