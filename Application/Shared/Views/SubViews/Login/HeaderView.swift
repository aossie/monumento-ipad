//
//  HeaderView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 29/06/21.
//

import SwiftUI

struct HeaderView: View {
    var session: SessionStore
    var body: some View {
        VStack(alignment:.leading,spacing:0) {
        Text(session.title)
            .font(.header)
            .lineLimit(1)
            .minimumScaleFactor(0.1)
        Text(session.subTitle)
            .font(.subHeader)
            .foregroundColor(.secondary)
        }
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView(session: SessionStore(mode: .login))
    }
}
