//
//  MonumentoHeader.swift
//  Monumento
//
//  Created by Aaryan Kothari on 15/07/21.
//

import SwiftUI

struct MonumentoHeader: View {
    var body: some View {
        GeometryReader{ gr in
            HStack {
                Text("Monumento")
                    .font(.system(size: 75))
                    .bold()
                    .foregroundColor(.customYellow)
                    .minimumScaleFactor(0.5)
                    .lineLimit(2)
                    .frame(width: gr.size.width/2)
                Spacer()
                Button(action: {}) {
                    Image(systemName: "bell")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(height: 30)
                }
                .buttonStyle(PlainButtonStyle())
            }
        }
        .frame(maxHeight:75)
        .padding(.top,20)
    }
}

struct MonumentoHeader_Previews: PreviewProvider {
    static var previews: some View {
        MonumentoHeader()
    }
}
