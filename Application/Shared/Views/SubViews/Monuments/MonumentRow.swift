//
//  MonumentRow.swift
//  Monumento
//
//  Created by Aaryan Kothari on 15/07/21.
//

import SwiftUI
import Kingfisher

struct MonumentRow: View {
    var monument: Monument
    var body: some View {
        ZStack(alignment: Alignment(horizontal: .leading, vertical: .bottom)) {
            KFImage(monument.image)
                .resizable()
                .cornerRadius(8)
                .scaledToFill()
               
                
            Text(monument.name ?? "")
                .font(.system(size: 40, weight: .bold))
                .foregroundColor(.white)
                .minimumScaleFactor(0.5)
                .lineLimit(1)
                .padding(.horizontal,30)
                .padding(.vertical,15)
                .padding(.trailing,50)
        }
        .aspectRatio(3, contentMode: .fill)
        .cornerRadius(8)
    }
}

struct MonumentRow_Previews: PreviewProvider {
    static var previews: some View {
        MonumentRow(monument: Monument.data[0])
            .previewLayout(.sizeThatFits)
    }
}
