//
//  CircularImage.swift
//  Monumento
//
//  Created by Aaryan Kothari on 17/07/21.
//

import SwiftUI
import Kingfisher

struct CircularImage: View {
    var url: URL?
    var height: CGFloat
    var image: UIImage?
    var body: some View {
        KFImage(url)
            .placeholder({
                if let image = image {
                Image(uiImage: image)
                    .resizable()
                    .scaledToFill()
                }
            })
            .resizable()
            .scaledToFill()
            .frame(width: height, height: height)
            .clipShape(Circle())
    }
}

struct CircularImage_Previews: PreviewProvider {
    static var previews: some View {
        CircularImage(url: URL(string: ""),height: 200)
    }
}
