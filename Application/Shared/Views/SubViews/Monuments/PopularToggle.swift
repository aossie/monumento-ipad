//
//  PopularToggle.swift
//  Monumento
//
//  Created by Aaryan Kothari on 16/07/21.
//

import SwiftUI

struct PopularToggle: View {
    @ObservedObject var monumentVM: MonumentViewModel
    
    var body: some View {
        HStack {
            Text(monumentVM.listHeader)
                .bold()
            Spacer()
            
            Button(action: monumentVM.toggleButton){
                Label(monumentVM.buttonTitle, systemImage: monumentVM.buttonImage)
            }
            .buttonStyle(PlainButtonStyle())
        }
    }
}

struct PopularToggle_Previews: PreviewProvider {
    static var previews: some View {
        PopularToggle(monumentVM: MonumentViewModel())
    }
}
