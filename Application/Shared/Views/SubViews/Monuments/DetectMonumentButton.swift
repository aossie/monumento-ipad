//
//  DetectMonumentButton.swift
//  Monumento
//
//  Created by Aaryan Kothari on 15/07/21.
//

import SwiftUI

struct DetectMonumentButton: View {
    var body: some View {
        HStack {
            Spacer()
            Image(systemName: "map")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .foregroundColor(.white)
                .shadow(color: .black, radius: 2)
                .padding(3)
            Text("DETECT MONUMENT")
                .font(.system(size: 40,weight: .bold))
                .foregroundColor(.white)
                .shadow(color: .black, radius: 2)
            Spacer()
        }
        .padding(21)
        .background(Color.customYellow)
        .roundedBorder(radius: 8, color: .black)
        .aspectRatio(8.8, contentMode: .fit)
        .padding(.vertical,40)
    }
}

struct DetectMonumentButton_Previews: PreviewProvider {
    static var previews: some View {
        DetectMonumentButton()
            .previewLayout(.sizeThatFits)
    }
}
