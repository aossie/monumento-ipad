//
//  MonumentList.swift
//  Monumento
//
//  Created by Aaryan Kothari on 16/07/21.
//

import SwiftUI

struct MonumentList: View {
    @ObservedObject var monumentVM: MonumentViewModel
    var body: some View {
        ScrollView {
            LazyVGrid(columns: monumentVM.columns) {
                ForEach(monumentVM.monuments) { monument in
                    NavigationLink(
                        destination: MonumentDetailView(monument: monument)) {
                    MonumentRow(monument: monument)
                    }
                }
            }
        }
    }
}

struct MonumentList_Previews: PreviewProvider {
    static var previews: some View {
        MonumentList(monumentVM: MonumentViewModel())
    }
}
