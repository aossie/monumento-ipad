//
//  TextField.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 29/06/21.
//

import SwiftUI

struct CustomTextFieldStyle: TextFieldStyle {
    
    var type: TextFieldType
    var gradient: Gradient
    
    init(type:TextFieldType,gradient: Gradient) {
        self.gradient = gradient
        self.type = type
    }
    
    func _body(configuration: TextField<Self._Label>) -> some View {
        HStack {
            gradient.topBottomLinear
                .mask (
                    Image(systemName: type.icon.rawValue)
                .resizable()
                .aspectRatio(contentMode: .fit)
                )
                .frame(width: 30, height:30)
            configuration
        }
        .padding(20)
        .roundedBorder(radius: 4, color: .secondary)
    }
}

struct CustomTextField: View {
    var type: TextFieldType
    var gradient: Gradient = Gradient(colors: [.customYellow,.customOrange])
    var isSecure: Bool = false
    @Binding var text: String
    var body: some View {
        if isSecure {
            SecureField(type.placeHolder.rawValue, text: $text)
                .textFieldStyle(CustomTextFieldStyle(type: type, gradient: gradient))
        } else {
            TextField(type.placeHolder.rawValue, text: $text)
                .textFieldStyle(CustomTextFieldStyle(type: type, gradient: gradient))
        }
    }
}
