//
//  LoginView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 28/06/21.
//

import SwiftUI

struct LoginView: View {
    
    // MARK: - PROPERTIES
    @ObservedObject var session: SessionStore
    @Binding var flip: Bool
    
    // MARK: - BODY
    var body: some View {
            
            VStack(alignment: .leading, spacing: 25) {
                
                HeaderView(session: session)
                
                CustomTextField(type: .email, gradient: session.background, text: $session.emailText)
                
                CustomTextField(type: .password, gradient: session.background,isSecure: true ,text: $session.passwordText)
                                
                ForgotPasswordRow(session: session)
                
                Button(session.buttonTitle, action: session.tappedActionButton)
                    .buttonStyle(CustomButtonStyle(gradient: session.background))
                
                SocialButtonRow(session: session)
                
                SwitchModeButton(session: session).button(action: flipCard)
             
            } // VSTACK
            .padding(.horizontal,27)
            .padding(.top,72)
            .padding(18)
            .background(Color.white)
            .cornerRadius(22.5)
            .shadow(color: .black.opacity(UIDevice.isIPad ? 0.5 : 0.0), radius: 25, x: 0, y: 10)
            .padding(.horizontal,DesignConstants.Login.horizontal)
            .alert(isPresented: $session.showAlert, content: {Alert.okayAlert(alert: session.alert) })
        
    }
    
    func flipCard() {
        withAnimation(Animation.linear(duration: 0.8)) {
        self.flip.toggle()
        }
    }
}

// MARK: - PREVIEW
struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LoginView(session: SessionStore(mode: .login), flip: .constant(false))
                .previewDevice("iPad Pro (11-inch) (3rd generation)")
            LoginView(session: SessionStore(mode: .login), flip: .constant(false))
                .previewDevice("iPhone 12")
        }
    }
}
