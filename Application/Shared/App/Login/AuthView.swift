//
//  AuthView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 01/07/21.
//

import SwiftUI

struct AuthView: View {
    
    // MARK: - PROPERTIES
    @State private var flipped = false
    @State private var animate3d = false
    @State private var colors: [Color] = [.customYellow, .customOrange]
    
    @StateObject var loginVM: SessionStore = SessionStore(mode: .login)
    @StateObject var signupVM: SessionStore = SessionStore(mode: .signup)
    
    // MARK: - BODY
    var body: some View {
        ZStack {
            
            if UIDevice.isIPad { Gradient(colors: colors).topBottomLinear }
            
            Group {
                LoginView(session: loginVM, flip: $animate3d).opacity(flipped ? 0.0 : 1.0)
                LoginView(session: signupVM, flip: $animate3d).opacity(flipped ? 1.0 : 0.0)
            }
            .modifier(FlipEffect(flipped: $flipped, angle: animate3d ? 180 : 0, axis: (x: 0, y: 1)))
            .onChange(of: flipped) { newValue in
                withAnimation(.easeInOut) {
                    self.colors = flipped ? [.lightGreen, .darkGreen] : [.customYellow, .customOrange]
                }
            }
        } // ZSTACK
        .edgesIgnoringSafeArea(.all)
    }
}

// MARK: - PREVIEW
struct AuthView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
    }
}
