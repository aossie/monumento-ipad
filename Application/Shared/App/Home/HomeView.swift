//
//  HomeView.swift
//  Monumento
//
//  Created by Aaryan Kothari on 01/07/21.
//

import SwiftUI

struct HomeView: View {
    var session: SessionStore = SessionStore(mode: .login)
    var body: some View {
        VStack {
        Button("Logout",action:session.signout)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
