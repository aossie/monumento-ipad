//
//  CommentsView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 24/07/21.
//

import SwiftUI

struct CommentsView: View {
    @ObservedObject var postVM: PostViewModel
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>

    var body: some View {
        VStack(alignment:.leading, spacing:20) {
            Image(systemName: StringConstants.SystemImages.x.rawValue)
                .resizable()
                .frame(width: 40, height: 40)
                .foregroundColor(.customOrange)
                .padding(.leading,-20)
                .padding(.top,30)
                .button {
                    self.mode.wrappedValue.dismiss()
                }
            
            Text("Comments")
                .font(.system(size: 50, weight: .bold))
                .foregroundColor(.customOrange)
            
            ForEach(postVM.comments) { comment in
                CommentRow(comment: comment)
            }
            
            Spacer()
            SendCommentView(postVM: postVM)
        }
        .padding([.bottom,.horizontal],50)
    }
}

struct CommentsView_Previews: PreviewProvider {
    static var previews: some View {
        CommentsView(postVM: PostViewModel(with: "1234"))
    }
}
