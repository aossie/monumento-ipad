//
//  FeedView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 23/07/21.
//

import SwiftUI

struct FeedView: View {
    @StateObject var socialVM: SocialViewModel = SocialViewModel()
    
    var body: some View {
        NavigationView{
            VStack(alignment: .leading) {
                HStack {
                    Text("Your Feed")
                        .font(.system(size: 70, weight: .bold))
                        .padding(.vertical,30)
                    Spacer()
                    Button(action:socialVM.showImagePicker){
                        Text("+")
                            .font(.system(size: 40))
                            .bold()
                            .foregroundColor(.customOrange)
                    }
                }
                .padding(.horizontal,20)
                ScrollView {
                    VStack(spacing:35) {
                        ForEach(socialVM.posts) { post in
                            FeedRow(post: post,postVM: PostViewModel(with: post.docId ?? ""))
                        }
                    }
                }
            }
            .padding(.horizontal,67)
        }
        .sheet(isPresented: $socialVM.showingImagePicker,onDismiss: socialVM.loadImage) {
            ImagePicker(image: $socialVM.inputImage, source: .photoLibrary)
        }
        .sheet(isPresented: $socialVM.presentAddPostView) {
            AddPostView(image: socialVM.inputImage!, socialVM: socialVM)
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct FeedView_Previews: PreviewProvider {
    static var previews: some View {
        FeedView()
    }
}
