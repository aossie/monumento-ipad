//
//  DiscoverView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 13/08/21.
//

import SwiftUI

struct DiscoverView: View {
    @ObservedObject var discoverVM = DiscoverViewModel()
    var body: some View {
        VStack(alignment:.leading, spacing:50) {
            Text("Discover")
                .font(.system(size: 75))
                .bold()
            
        CustomTextField(type: .search, text: $discoverVM.query)
            VStack(spacing: 25) {
            ForEach(discoverVM.searchUsers) { user in
                NavigationLink(
                    destination: ProfileView(profileVM: ProfileViewModel(uid: user.uid ?? ""))) {
                DiscoverRow(user: user)
                }
            }
            }
            Spacer()
            
            if discoverVM.searchUsers.isEmpty {
                HStack {
                    Spacer()
                Text("No Search Results")
                    Spacer()
                }
                Spacer()
            }
        }
        .padding(.horizontal,30)
    }
}


struct DiscoverView_Previews: PreviewProvider {
    static var previews: some View {
        DiscoverView()
    }
}
