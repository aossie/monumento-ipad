//
//  ARView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 18/08/21.
//

import SwiftUI
import SceneKit


struct ARView: View {
    
    var scene = SCNScene(named: "tajmahal.obj")
    
    var cameraNode: SCNNode? {
      scene?.rootNode.childNode(withName: "camera", recursively: false)
      }
    
    var body: some View {
        VStack {
            Text("Hello World")
        SceneView(
              scene: scene,
              pointOfView: cameraNode,
              options: [.allowsCameraControl,.autoenablesDefaultLighting]
        )
        }
    }
}

struct ARView_Previews: PreviewProvider {
    static var previews: some View {
        ARView()
    }
}
