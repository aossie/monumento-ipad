//
//  ProfileView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 28/07/21.
//

import SwiftUI
import Kingfisher

struct ProfileView: View {
    
    @StateObject var profileVM: ProfileViewModel
    
    var body: some View {
        VStack(spacing:30) {
            ZStack {
                Gradient(colors: [.customOrange,.customYellow]).topBottomLinear
                    .clipShape(Circle())
                    .aspectRatio(1, contentMode: .fit)
                    .frame(width: 232, height: 232)
            KFImage(profileVM.profile.profilePictureUrl)
                .resizable()
                .scaledToFill()
                .clipShape(Circle())
                .frame(width: 225, height: 225)
            }
            
            Text(profileVM.profile.name ?? "")
                .font(.system(size: 50, weight: .heavy))
            
            Text(profileVM.profile.formattedUserName)
                .font(.system(size: 30, weight: .bold))
                .foregroundColor(.secondary)
                .offset(y:-20)

            HStack(spacing: 35) {
                
                Button(action: profileVM.followUser) {
                
                    HStack {
                        Spacer()
                        Text(profileVM.followTitle)
                            .bold()
                            .foregroundColor(.black)
                        Spacer()
                    }
                    .frame(height: 50)
                    .background(Color.customYellow)
                }
                
                Button(action: {}) {
                    Image(StringConstants.Images.share2.rawValue)
                        .resizable()
                        .scaledToFill()
                        .padding(15)
                        .background(Color.customOrange)
                        .frame(width: 50, height: 50)
                }

            }
            .cornerRadius(5)
            
            ScrollView {
                LazyVGrid(columns: profileVM.columns,spacing:40) {
                    ForEach(profileVM.posts) { post in
                        KFImage(post.imageUrl)
                            .resizable()
                            .cornerRadius(8)
                            .scaledToFill()
                    }
                }
            }
            Spacer()
        }
        .padding(.horizontal,50)
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(profileVM: ProfileViewModel(uid: ""))
    }
}
