//
//  AddPhotoView.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 01/08/21.
//

import SwiftUI

struct AddPostView: View {
    var image: UIImage
    @ObservedObject var socialVM: SocialViewModel
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>

    var body: some View {
        VStack(alignment:.leading,spacing:30) {
            Image(systemName: StringConstants.SystemImages.x.rawValue)
                .resizable()
                .frame(width: 40, height: 40)
                .foregroundColor(.customOrange)
                .button {
                    self.mode.wrappedValue.dismiss()
                }
            
            Image(uiImage: image)
                .resizable()
                .scaledToFit()
            
            
            CustomTextField(type: .title, text: $socialVM.title)
            
            CustomTextField(type: .location, text: $socialVM.location)
            
            CustomButton(title: "Post",action: socialVM.addPost)
            
            Spacer(minLength: 40)
        }
        .onReceive(socialVM.$posted, perform: { posted in
            if posted {
                self.mode.wrappedValue.dismiss()
            }
        })
        .padding(50)
    }
}

struct AddPostView_Previews: PreviewProvider {
    static var previews: some View {
        AddPostView(image: UIImage(named: "cor")!, socialVM: SocialViewModel())
    }
}
