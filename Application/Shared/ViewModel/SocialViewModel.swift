//
//  SocialViewModel.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 23/07/21.
//

import UIKit
import SwiftUI
import Firebase


class SocialViewModel: ObservableObject {
    @Published var socialService: PostService = PostService()
    @Published var posts: [Post] = []
    
    @Published var inputImage : UIImage?
    @Published var showingImagePicker: Bool = false
    @Published var presentAddPostView: Bool = false
    
    @Published var title: String = ""
    @Published var location: String = ""
    @Published var posted: Bool = false
    
    init() {
        self.getFeed()
    }
    
    func getFeed() {
        socialService.fetchPosts {
            self.posts = $0
        }
    }
    
    func loadImage(){
        guard let inputImage = inputImage else { return }
        self.inputImage = inputImage
        self.presentAddPostView = true
    }
    
    func showImagePicker() {
        self.showingImagePicker = true
    }
    
    func addPost() {
        guard let data = inputImage?.jpegData(compressionQuality: 0.5) else {
            print("Image not found")
            return
        }
        socialService.uploadPhoto(for: data) { image in
            let post = Post(author: User.current, image: image, location: self.location, timeStamp: Date().timeIntervalSince1970, title: self.title)
            self.socialService.addPost(for: post)
            self.posts.insert(post, at: 0)
            self.posted = true
        }
    }
}

class PostViewModel: ObservableObject {
    @Published var socialService: PostService = PostService()
    @Published var comments: Comments = []
    
    @Published var comment: String = "" {
        didSet {
            self.enabled = !comment.isEmpty
        }
    }
    
    @Published var enabled: Bool = false
    
    var postId: String
    
    init(with postId:String) {
        self.postId = postId
        self.getComments()
    }
    
    func getComments() {
        socialService.fetchComments(for: postId) {
            self.comments = $0
        }
    }
    
    func addComment() {
        let comment = Comment(id: UUID(), author: User.current, post: postId, comment: comment, timeStamp: Date().timeIntervalSince1970)
        socialService.sendComment(comment)
        
        self.comments.append(comment)
        self.comment = ""
    }
}

