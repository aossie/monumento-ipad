//
//  AuthViewModel.swift
//  Monumento
//
//  Created by Aaryan Kothari on 01/07/21.
//

import Firebase
import SwiftUI
import Combine

// MARK: - AUTHENTICATION VIEW MODEL

class SessionStore: ObservableObject {
    
    /// PUBLISHED VARIABLES
    @Published var mode: Mode
    @Published var emailText = ""
    @Published var passwordText = ""
    @Published var isValid = false
    @Published var rememberMe = false
    @Published var showAlert: Bool = false
    @Published var alert: StringConstants.Alerts = .loginSuccess
    
    @Published var userService: UserService = UserService()
    @Published var userExists: Bool = false
    
    private var cancellables: [AnyCancellable] = []

    
    init(mode: Mode) {
        self.mode = mode
    }
    
    var title: String {
        switch mode {
        case .login:
            return "Welome back 😉"
        case .signup:
            return "Hello 🍀"
        }
    }
    
    var subTitle: String {
        switch mode {
        case .login:
            return "Login with your email"
        case .signup:
            return "Create an account"
        }
    }
    
    var buttonTitle: String {
        switch mode {
        case .login:
            return "LOGIN"
        case .signup:
            return "SIGNUP"
        }
    }
    
    var screenTitle: String {
        switch mode {
        case .login:
            return "Signup"
        case .signup:
            return "Login"
        }
    }
    
    var switchModeMessage: String {
        switch mode {
        case .login:
            return "Don’t have an account? "
        case .signup:
            return "Already registered? "
        }
    }
    
    var background: Gradient {
        switch mode {
        case .login:
            return Gradient(colors: [.customYellow, .customOrange])
        case .signup:
            return Gradient(colors: [.lightGreen, .darkGreen])
        }
    }
    
    var primaryColor: Color {
        switch mode {
        case .login:
            return .customOrange
        case .signup:
            return .darkGreen
        }
    }
    
    func tappedActionButton() {
        print("EMAIL:\(emailText) PW:\(passwordText)")
        switch mode {
        case .login:
            AuthService.login(email: emailText, password: passwordText)
                .sink(receiveCompletion: setAlert(completion:), receiveValue: checkUID(uid:))
                .store(in: &cancellables)
            
        case .signup:
            AuthService.signup(email: emailText, password: passwordText)
                .sink(receiveCompletion: setAlert(completion:), receiveValue: checkUID(uid:))
                .store(in: &cancellables)
        }
    }
    
    func checkUID(uid: String) {
        userService.checkUser(id: uid) {
            print("User Exists: \($0)")
            UserDefaults.standard.set($0, forKey: "userExists")
        }
    }
    
    private func setAlert(completion: (Subscribers.Completion<Error>)) {
        switch completion {
        case .finished:
            self.showAlert = true
            self.alert = .loginSuccess
        case let .failure(error):
            self.showAlert = true
            self.alert = .error(message: error.localizedDescription)
        }
    }
    
    
    func socialSignin(with credential: AuthCredential) {
    AuthService.socialSignin(with: credential)
        .sink(receiveCompletion: setAlert(completion:), receiveValue: { _ in })
        .store(in: &cancellables)
    }
    
    func forgotPassword() {
    
    }
    
    func signout() {
        UserDefaults.standard.set(false, forKey: "userExists")
        AuthService.logout()
            .sink(receiveCompletion: setAlert(completion:), receiveValue:{ _ in })
            .store(in: &cancellables)
    }
    
    
}

extension SessionStore {
    enum Mode {
        case login
        case signup
    }
}
