//
//  SessionStore.swift
//  Monumento (iOS)
//
//  Created by Aaryan Kothari on 28/06/21.
//

import Firebase
import SwiftUI
import Combine

class AuthListener: ObservableObject {
    
    ///  PRIVATE VARIABLES
    private var didChange = PassthroughSubject<AuthListener,Never>()
    private var handle : AuthStateDidChangeListenerHandle?

    /// PUBLISHED VARIABLES
    @Published var session : Session? {
         didSet { self.didChange.send(self) }
     }
    
    func listen() {
        handle = Auth.auth().addStateDidChangeListener(listener)
    }
    
    private func listener(auth:Auth,user:Firebase.User?) {
        guard let user = user else {
            self.session = nil
            return
        }
        self.session = Session(uid: user.uid, email: user.email)
        print("AUTHENTICATED: \(session)")
    }
    
}
