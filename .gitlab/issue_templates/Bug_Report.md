---
name: Bug Report 🐞
about: Something isn't working as expected? Here is the right place to report.
labels: "type: bug"
---

<!--
  Please fill out each section below, otherwise, your issue will be closed. This info allows the maintainers to diagnose (and fix!) your issue as quickly as possible.
-->

## Description

A clear and concise description of the issue you are facing.

### Steps to reproduce

Steps to reproduce the behavior:
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'
4. See error

### Expected result

A clear and concise description of what you expected to happen.

### Screenshots [ Optional ]

Add screenshots to help explain your problem.

### Environment

please complete the following information:
 - Device: [e.g. iPhone12]
 - OS: [e.g. iOS 14.5]
 - Xcode [e.g. Xcode 12.5]

### Additional context**

Add any other context about the problem here.
