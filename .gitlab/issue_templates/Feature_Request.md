---
Title: Feature Request 💡
Description: Suggest a new idea for the project.
labels: "type: feature or enhancement"
---

<!--
  Please fill out each section below, otherwise, your issue will be closed.

  Before opening a new issue, please search existing issues
  
  Don't be afraid to add a feature request!
-->

## Summary

A brief explanation of the feature.

### Basic code example [ Optional ]

If the proposal involves a major change in the codes structure, include a basic code example along with a short explaination justifying the changes.

### Motivation

Why are we doing this? What use cases does it support? What is the expected outcome?

### Additional context

Add any other context or screenshots about the feature request here.
